Title: Technology Equity Project
Date:  2018-07-12 1:40
Category: Announcement

Announcement Coming Soon
=========

In the meantime, please read the articles below for a discussion
of the technology gap and the impact on low income communities

<http://connectyourcommunity.org/2016-census-data-cleveland-still-3rd-worst-connected-big-city/>

<https://www.technologyreview.com/s/531726/technology-and-inequality/>

<https://www.technologyreview.com/s/603083/the-unacceptable-persistence-of-the-digital-divide/>
